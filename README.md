# README #

Map/Reduce para encontrar el número de palabras que hay en un dataset ordenado por nombre de documento.

## Ejecución
1. Clonar el repositorio en el cluster de hadoop.
2. Crear una colección en MongoDB
3. Editar la configuración de la base de datos en el script.
4. Ejecutar el script de python:

        python script.py hdfs:///datasets/*.txt -r hadoop --output-dir hdfs:///user/{USER}/data_out/out

5. El resultado se encontrará en `hdfs:///user/{USER}/data_out/out` y en la base de datos.